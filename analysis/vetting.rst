Candidate Vetting
=================

:doc:`GWCelery <gwcelery:index>` orchestrates and supervises performing basic
data quality and detector state checks, grouping of events from individual
pipelines into :doc:`superevents <superevents>`, initiating automated :doc:`sky
localization and parameter estimation <parameter_estimation>`, inferring
:doc:`classification and source properties <inference>`, and sending alerts.

A Data Quality Report is prepared that consists of a semi-automated detector
characterization and data quality investigation for each **significant** event.
(See :ref:`alert-threshold` for an explanation of what constitutes a
significant event.)
It provides a variety of metrics based on auxiliary instrumental and
environmental sensors that are used by the rapid response team (:term:`RRT`) in
order to make a decision of whether to confirm or retract a candidate.

All **significant** candidates are handled by non-expert :term:`RRT` members at
first. Expert members of the team will only be convened as necessity arises.

However, when the candidate satisfies at least one of the following conditions,
all RRT members including the experts will start investigating
as soon as the public alert is issued to provide a higher level of scrutiny.

#. Coincidence with non-GW events. (See :ref:`coinc-external-trigger`.)
#. A **significant** unmodeled burst trigger has the lowest false alarm rate in
   a :doc:`superevent <superevents>`. (See :ref:`unmodeled`.)
#. A :term:`CBC` trigger is :ref:`preferred <preferred-event>` AND has less
   than 0.5 probability of being terrestrial AND satisfies at least one of the
   following conditions.

   * Probability that the candidate is BNS is larger than 0.1.
   * Probability that the candidate is NSBH is larger than 0.1.
   * Probability that some neutron star material remains outside the final
     black hole remnant is larger than 0.1.
   * 90% credible region in the sky localization is smaller than 100 square
     degrees.

If one of these conditions is met at the time of the first or the second
:ref:`Preliminary Notice <preliminary>`,
the candidate will be automatically labeled ``HIGH_PROFILE`` on GraceDB.
(See :doc:`GraceDB Reference Manual
<gracedb:labels>`.)

Once applied, the label will not be removed regardless of the later findings,
even if the candidate is retracted.

In rare cases, RRT might manually apply ``HIGH_PROFILE`` label to superevents
for reasons other than the conditions listed above. In such cases, a brief
explanation will be given in the Circular.
